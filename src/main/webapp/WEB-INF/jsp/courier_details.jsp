<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com -->
    <title>Ecom Courier</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/courier_details.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-color: grey">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">Amazon Courier Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <%--<li><a href="/home">DASHBOARD</a></li>--%>
                <li><a href="#">${userName}</a></li>
                <li><a href="/dashboard">DASHBOARD</a></li>
                <li><a href="/home">REGISTER-ORDER</a></li>
                    <li><a href="/logout-success">LOGOUT</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Container (Dashboard Section) -->
<div id="dashboard-details" class="container-fluid text-center">
    <div class="row panel-heading">
        <div class="col-sm-12">
            <h2>COURIER DETAILS STATE</h2><br>
        </div>
    </div>
    <div class="formdesign text-center">
        <div class="row">
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="source">CourierID</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="courierId" name="courierId" type="text" value=${courierDetails.state.data.courierId} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="source">Requestor</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="requestor" name="requestor" type="text" value=${courierDetails.state.data.requestor} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="source">Source Adddress</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="source" name="source" type="text" value=${courierDetails.state.data.source} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="destination">Destination Adddress</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="destination" name="destination"  type="text" value=${courierDetails.state.data.destination} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="length">CourierLength</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="length" name="length" type="text" value=${courierDetails.state.data.courierLength} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="width">CourierWidth</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="width" name="width" type="text" value=${courierDetails.state.data.courierWidth} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="height">CourierHeight</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="height" name="height" type="text" value=${courierDetails.state.data.courierHeight} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group left">
                    <label for="weight">CourierWeight</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="weight" name="weight" type="text"  value=${courierDetails.state.data.courierWeight} readonly>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="status">Status</label>
                </div>
                <div class="col-sm-6 form-group">
                    <input class="form-control" id="status" name="status" type="text" value=${courierDetails.state.data.status} readonly>
                </div>
            </div>
            <br>
            <c:if test="${courierDetails.state.data.status =='accepted'}">
                <div class="row">
                    <div class="col-sm-4 form-group"></div>
                    <div class="col-sm-8 form-group">
                        <form method="post" action="/upload-image">
                            <div class="input-group">
                                <div>
                                    <%--<button type="submit" class="btn  btn-block btn-success">UPLOAD IMAGE</button>--%>
                                    <input type="file" name="pic" accept="image/*">
                                    <input type="submit" class="btn  btn-block btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </c:if>
            <br>
            <c:if test="${courierDetails.state.data.status !='initiated'}">
                <div class="row table-courier-details">
                    <p align="center">
                    <c:if test="${courierDetails.state.data.status =='response-received'}">
                        <table class="table table-bordered">
                            <thread>
                                <tr align="center">
                                    <th>Name</th>
                                    <th>Shared Price</th>
                                    <th>Dedicated Price</th>
                                    <th>finalDeliveryType</th>
                                    <th>Accept</th>
                                </tr>
                            </thread>
                            <tbody>
                                <c:forEach items="${responses}" var="temp">
                                    <form id="2" method="post" action="/accept-reject" align="center">
                                        <tr>
                                            <td>${temp.name}</td>
                                            <td>${temp.sharedPrice}</td>
                                            <td>${temp.dedicatedPrice}</td>
                                            <td>
                                                <select class="form-control" id="finalDeliveryType1" name="finalDeliveryType">
                                                    <option value="shared">Shared</option>
                                                    <option value="dedicated">Dedicated</option>
                                                </select>
                                            </td>
                                            <input class="form-control" id="courierId" name="courierId" type="hidden" value=${courierDetails.state.data.courierId}>
                                            <input class="form-control" id="responder" name="responder" type="hidden" value=${temp.name}>
                                            <%--<input class="form-control" id="finalDeliveryType" name="finalDeliveryType" type="hidden" value=${courierDetails.state.data.finalDeliveryType}>--%>
                                                <c:if test="${courierDetails.state.data.status =='response-received'}">
                                                    <td><button class="btn btn-success btn-block" value="accept" name="approval" type="submit">Accept</button></td>
                                                </c:if>
                                        </tr>
                                    </form>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <c:if test="${courierDetails.state.data.status !='response-received'}">
                        <table class="table table-bordered">
                            <thread>
                                <tr align="center">
                                    <th>Responder</th>
                                    <th>finalQuotedPrice</th>
                                    <th>finalDeliveryType</th>
                                    <th>Action</th>
                                </tr>
                            </thread>
                            <tbody>
                                <form id="3" method="post" action="/accept-reject" align="center">
                                    <tr>
                                        <td>${courierDetails.state.data.acceptedResponder}</td>
                                        <td>${courierDetails.state.data.finalQuotedPrice}</td>
                                        <td>${courierDetails.state.data.finalDeliveryType}</td>
                                        <input class="form-control" id="courierId" name="courierId" type="hidden" value=${courierDetails.state.data.courierId}>
                                        <input class="form-control" id="responder" name="responder" type="hidden" value=${courierDetails.state.data.acceptedResponder}>
                                        <input class="form-control" id="finalDeliveryType" name="finalDeliveryType" type="hidden" value=${courierDetails.state.data.finalDeliveryType}>
                                        <c:if test="${courierDetails.state.data.status =='accepted'}">
                                            <td><button class="btn btn-danger btn-block" value="reject" name="approval" type="submit">Reject</button></td>
                                        </c:if>
                                        <c:if test="${courierDetails.state.data.status =='uploaded'}">
                                            <td><button class="btn btn-danger btn-block" value="reject" name="approval" type="submit">Reject</button></td>
                                        </c:if>
                                        <c:if test="${courierDetails.state.data.status =='picked'}">
                                            <td><button class="btn btn-danger btn-block" value="reject" name="approval" type="submit" disabled>Reject</button></td>
                                        </c:if>
                                        <c:if test="${courierDetails.state.data.status =='delivered'}">
                                            <td><button class="btn btn-danger btn-block" value="reject" name="approval" type="submit" disabled>Reject</button></td>
                                        </c:if>
                                    </tr>
                                </form>
                            </tbody>
                        </table>
                    </c:if>
                </div>
            </c:if>
        </div>
    </div>
</div>


<%--<c:if test="${courierDetails.state.data.status =='accepted'}">--%>
    <%--<td><button class="btn btn-success btn-block" value="accept" name="approval" type="submit" disabled>Accept</button></td>--%>
    <%--<td><button class="btn btn-danger btn-block" value="reject" name="approval" type="submit">Reject</button></td>--%>
<%--</c:if>--%>
<%--<c:if test="${courierDetails.state.data.status =='pickedup'}">--%>
    <%--<td><button class="btn btn-success btn-block" value="accept" name="approval" type="submit" disabled>Accept</button></td>--%>
    <%--<td><button class="btn btn-danger btn-block" value="reject" name="approval" type="submit" disabled>Reject</button></td>--%>
<%--</c:if>--%>

<footer class="container-fluid text-center">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
</footer>

<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>

</body>
</html>