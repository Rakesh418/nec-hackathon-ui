<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com -->
    <title>Ecom Courier</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <style>
    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-color: grey">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">Amazon Courier Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">${userName}</a></li>
                <li><a href="/home">DASHBOARD</a></li>
                <li><a href="/home">REGISTER-ORDER</a></li>
                    <li><a href="/logout-success">LOGOUT</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Container (Dashboard Section) -->
<div id="dashboard-details" class="container-fluid text-center">
    <div class="row panel-heading">
        <div class="col-sm-12">
            <h2>ALL COURIERS DETAILS</h2><br>
        </div>
    </div>
    <div class="dashboard text-center">
        <div align="center">
            <table class="table table-bordered">
                <thread>
                    <tr align="center">
                        <th>CourierId</th>
                        <th>Source</th>
                        <th>Destination</th>
                        <th>Status</th>
                        <th>Details</th>
                    </tr>
                </thread>
                <tbody>
                    <c:forEach items="${list}" var="temp">
                        <form id="2" method="get" action="/courier-details" align="center">
                            <tr>
                                <td>${temp.state.data.courierId}</td>
                                <td>${temp.state.data.source}</td>
                                <td>${temp.state.data.destination}</td>
                                <td>${temp.state.data.status}</td>
                                <td>
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn  btn-block btn-info">Details</button>
                                    </div>
                                </td><input class="form-control" id="courierId" name="courierId" type="hidden" value=${temp.state.data.courierId}>
                            </tr>
                        </form>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
</footer>

<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>

</body>
</html>