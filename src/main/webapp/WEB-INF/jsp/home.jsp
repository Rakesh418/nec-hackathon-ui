<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Theme Made By www.w3schools.com -->
    <title>Amazon Courier</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <%--<style>--%>

    <%--</style>--%>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-color: grey">

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#myPage">Amazon Courier Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">${userName}</a></li>
                <li><a href="#dashboard">DASHBOARD</a></li>
                <li><a href="#register-order">REGISTER-ORDER</a></li>
                <li><a href="/logout-success">LOGOUT</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Container (Dashboard Section) -->
<div id="dashboard" class="container-fluid text-center">
    <div class="row panel-heading color-bar">
        <div class="col-sm-12">
            <h2>DASHBOARD DETAILS</h2><br>
        </div>
    </div>
    <div class="dashboard">
    <form method="get" action="/dashboard">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="submit" class="btn  btn-block btn-danger">DASHBOARD</button>
            </div>
        </div>
    </form>
    </div>
</div>

<div id="register-order" class="container-fluid text-center">
    <div class="row panel-heading color-bar">
        <div class="col-sm-12">
            <h2>COURIER REGISTRATION FORM</h2><br>
        </div>
    </div>
    <div class="formdesign text-center">
        <form method="post" action="/register-order">
            <div class="row">
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label for="source">Source Adddress</label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="source" name="source" placeholder="source" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label for="destination">Destination Adddress</label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="destination" name="destination" placeholder="destination" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label for="courierLength">CourierLength</label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="courierLength" name="courierLength" placeholder="courierLength" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label for="courierWidth">CourierWidth</label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="courierWidth" name="courierWidth" placeholder="courierWidth" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label for="courierHeight">CourierHeight</label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="courierHeight" name="courierHeight" placeholder="courierHeight" type="text" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group left">
                        <label for="courierWeight">CourierWeight</label>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="courierWeight" name="courierWeight" placeholder="courierWeight" type="text" required>
                    </div>
                </div>
                <input class="form-control" id="partyName" name="partyName" placeholder="partyName" type="hidden" value="O=NECAuto,L=New%20York,C=US" required>
                <div class="row">
                    <p align="center">
                    <div class="col-sm-4 form-group"></div>
                    <div class="col-sm-6 form-group">
                        <button class="btn btn-danger btn-block" type="submit">Register</button>
                    </div>
                    <div class="col-sm-2 form-group"></div>
                </div>
            </div>
        </form>
    </div>
</div>

<footer class="container-fluid text-center">
    <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a>
</footer>

<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 900, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>

</body>
</html>