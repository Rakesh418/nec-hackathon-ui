<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Admin Console for Amazon</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=0">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="img/favicon.png">
<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
<link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Related styles of various icon packs and plugins -->
<link rel="stylesheet" href="css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="css/main.css">

<!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
<link rel="stylesheet" href="css/themes.css">
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) -->
<script src="js/vendor/modernizr.min.js"></script>
</head>
<body>
	<!-- Page Wrapper -->
	<!-- In the PHP version you can set the following options from inc/config file -->
	<!--
            Available classes:

            'page-loading'      enables page preloader
        -->
	<div id="page-wrapper">
		<div id="page-container"
			class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

			<!-- Main Sidebar -->
			<div id="sidebar">
				<!-- Wrapper for scrolling functionality -->
				<div id="sidebar-scroll">
					<!-- Sidebar Content -->
					<div class="sidebar-content">
						<!-- Brand -->
						<a href="index.html" class="sidebar-brand"> <i
							class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Admin</strong>
								Console</span>
						</a>
						<!-- END Brand -->

						<!-- User Info -->
						<div
							class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
							<div class="sidebar-user-avatar">
								<img src="img/placeholders/avatars/avatar2.jpg" alt="avatar">
							</div>
							<div class="sidebar-user-name">Amazon</div>
							<div class="sidebar-user-links">
								<!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
								<a href="javascript:void(0)" class="enable-tooltip"
									data-placement="bottom" title="Settings"
									onclick="$('#modal-user-settings').modal('show');"><i
									class="gi gi-cogwheel"></i></a> <a href="login.html"
									data-toggle="tooltip" data-placement="bottom" title="Logout"><i
									class="gi gi-exit"></i></a>
							</div>
						</div>
						<!-- END User Info -->

						<!-- Theme Colors -->
						<!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
						<ul
							class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">
							<!-- You can also add the default color theme
                                    <li class="active">
                                        <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                                    </li>
                                    -->
							<li><a href="javascript:void(0)"
								class="themed-background-dark-night themed-border-night"
								data-theme="css/themes/night.css" data-toggle="tooltip"
								title="Night"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-amethyst themed-border-amethyst"
								data-theme="css/themes/amethyst.css" data-toggle="tooltip"
								title="Amethyst"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-modern themed-border-modern"
								data-theme="css/themes/modern.css" data-toggle="tooltip"
								title="Modern"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-autumn themed-border-autumn"
								data-theme="css/themes/autumn.css" data-toggle="tooltip"
								title="Autumn"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-flatie themed-border-flatie"
								data-theme="css/themes/flatie.css" data-toggle="tooltip"
								title="Flatie"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-spring themed-border-spring"
								data-theme="css/themes/spring.css" data-toggle="tooltip"
								title="Spring"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-fancy themed-border-fancy"
								data-theme="css/themes/fancy.css" data-toggle="tooltip"
								title="Fancy"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-fire themed-border-fire"
								data-theme="css/themes/fire.css" data-toggle="tooltip"
								title="Fire"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-coral themed-border-coral"
								data-theme="css/themes/coral.css" data-toggle="tooltip"
								title="Coral"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-lake themed-border-lake"
								data-theme="css/themes/lake.css" data-toggle="tooltip"
								title="Lake"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-forest themed-border-forest"
								data-theme="css/themes/forest.css" data-toggle="tooltip"
								title="Forest"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-waterlily themed-border-waterlily"
								data-theme="css/themes/waterlily.css" data-toggle="tooltip"
								title="Waterlily"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-emerald themed-border-emerald"
								data-theme="css/themes/emerald.css" data-toggle="tooltip"
								title="Emerald"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-blackberry themed-border-blackberry"
								data-theme="css/themes/blackberry.css" data-toggle="tooltip"
								title="Blackberry"></a></li>
						</ul>
						<!-- END Theme Colors -->


						<!-- Sidebar Navigation -->
						<ul class="sidebar-nav">
							<li><a href="/dashboard"><i
									class="gi gi-stopwatch sidebar-nav-icon"></i><span
									class="sidebar-nav-mini-hide">Dashboard</span></a></li>

							<li><a href="/orders">Orders</a></li>
							<li><a href="">Order View</a></li>
							<li><a href="/create_courier" class=" active">Create
									Courier</a></li>
						</ul>
						<!-- END Sidebar Navigation -->
					</div>
					<!-- END Sidebar Content -->
				</div>
				<!-- END Wrapper for scrolling functionality -->
			</div>
			<!-- END Main Sidebar -->

			<!-- Main Container -->
			<div id="main-container">
				<!-- Header -->
				<!-- In the PHP version you can set the following options from inc/config file -->
				<!--
                        Available header.navbar classes:

                        'navbar-default'            for the default light header
                        'navbar-inverse'            for an alternative dark header

                        'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                            'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                        'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                            'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                    -->
				<header class="navbar navbar-default">
					<!-- Left Header Navigation -->
					<ul class="nav navbar-nav-custom">
						<!-- Main Sidebar Toggle Button -->
						<li><a href="javascript:void(0)"
							onclick="App.sidebar('toggle-sidebar');this.blur();"> <i
								class="fa fa-bars fa-fw"></i>
						</a></li>
						<!-- END Main Sidebar Toggle Button -->

						<!-- Template Options -->
						<!-- Change Options functionality can be found in js/app.js - templateOptions() -->
						<li class="dropdown"><a href="javascript:void(0)"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="gi gi-settings"></i>
						</a>
							<ul class="dropdown-menu dropdown-custom dropdown-options">
								<li class="dropdown-header text-center">Header Style</li>
								<li>
									<div class="btn-group btn-group-justified btn-group-sm">
										<a href="javascript:void(0)" class="btn btn-primary"
											id="options-header-default">Light</a> <a
											href="javascript:void(0)" class="btn btn-primary"
											id="options-header-inverse">Dark</a>
									</div>
								</li>
								<li class="dropdown-header text-center">Page Style</li>
								<li>
									<div class="btn-group btn-group-justified btn-group-sm">
										<a href="javascript:void(0)" class="btn btn-primary"
											id="options-main-style">Default</a> <a
											href="javascript:void(0)" class="btn btn-primary"
											id="options-main-style-alt">Alternative</a>
									</div>
								</li>
							</ul></li>
						<!-- END Template Options -->
					</ul>
					<!-- END Left Header Navigation -->

					<!-- Search Form -->
					<form action="page_ready_search_results.html" method="post"
						class="navbar-form-custom">
						<div class="form-group">
							<input type="text" id="top-search" name="top-search"
								class="form-control" placeholder="Search..">
						</div>
					</form>
					<!-- END Search Form -->

					<!-- Right Header Navigation -->
					<ul class="nav navbar-nav-custom pull-right">
					</ul>
					<!-- END Right Header Navigation -->
				</header>
				<!-- END Header -->

				<!-- Page content -->
				<div id="page-content">
					<!-- Dashboard Header -->
					<div class="content-header">
						<ul class="nav-horizontal text-center">
							<li><a href="/dashboard"><i class="fa fa-bar-chart"></i>
									Dashboard</a></li>
							<li><a href="/orders"><i class="gi gi-shop_window"></i>
									Orders</a></li>
							<li><a href=""><i class="gi gi-shopping_cart"></i> Order
									View</a></li>
							<li class="active"><a href="/create_courier"><i
									class="gi gi-pencil"></i> Create Courier</a></li>
						</ul>
					</div>
					<!-- END Dashboard Header -->

					<!-- Product Edit Content -->
					<div class="row">
						<div class="col-lg-6">
							<!-- General Data Block -->
							<div class="block">
								<!-- General Data Title -->
								<div class="block-title">
									<h2>
										<i class="fa fa-pencil"></i> <strong>Courier</strong> Data
									</h2>
								</div>
								<!-- END General Data Title -->

								<!-- General Data Content -->
								<form action="/register-order" method="post"
									class="form-bordered">
									<div class="form-group">
										<input type="text" id="qpost-title" name="source"
											class="form-control" placeholder="Source Adddress">
									</div>
									<div class="form-group">
										<input type="text" id="qpost-title" name="destination"
											class="form-control" placeholder="Destination Adddress">
									</div>

									<div class="form-group">
										<label class="sr-only" for="example-if-email">courierLength</label>
										<input type="text" id="example-if-email" name="courierLength"
											class="form-control" placeholder="courierLength">
									</div>
									<div class="form-group">
										<label class="sr-only" for="example-if-password">courierWidth</label>
										<input type="text" id="example-if-password"
											name="courierWidth" class="form-control"
											placeholder="courierWidth">
									</div>
									<div class="form-group">
										<label class="sr-only" for="example-if-password">courierHeight</label>
										<input type="text" id="example-if-password"
											name="courierHeight" class="form-control"
											placeholder="courierHeight">
									</div>

									<div class="form-group">
										<input type="text" id="qpost-title" name="courierWeight"
											class="form-control" placeholder="Courier Weight">
									</div>

									<input class="form-control" id="partyName" name="partyName"
										placeholder="partyName" type="hidden"
										value="O=NECAuto,L=New%20York,C=US" required>
									<div class="form-group form-actions">
										<button type="submit" class="btn btn-sm btn-primary">
											<i class="fa fa-check"></i> Submit
										</button>
										<button type="reset" class="btn btn-sm btn-warning">
											<i class="fa fa-repeat"></i> Reset
										</button>
									</div>
									<c:if test="${success-message == '1'}">
										<div class="alert alert-success alert-dismissable">
											<button type="button" class="close" data-dismiss="alert"
												aria-hidden="true">�</button>
											<h4>
												<i class="fa fa-check-circle"></i> Successfully
											</h4>
											Posted!
										</div>
									</c:if>
									<c:if test="${success-message == '2'}">
										<div class="alert alert-danger alert-dismissable">
											<button type="button" class="close" data-dismiss="alert"
												aria-hidden="true">�</button>
											<h4>
												<i class="fa fa-times-circle"></i> Error
											</h4>
											Oh no! Post failed!
										</div>
									</c:if>
								</form>
								<!-- END General Data Content -->
							</div>
							<!-- END General Data Block -->
						</div>
					</div>
					<!-- END Product Edit Content -->
				</div>
				<!-- END Page Content -->

				<!-- Footer -->
				<footer class="clearfix"> </footer>
				<!-- END Footer -->
			</div>
			<!-- END Main Container -->
		</div>
		<!-- END Page Container -->
	</div>
	<!-- END Page Wrapper -->

	<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
	<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

	<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
	<div id="modal-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> Settings
					</h2>
				</div>
				<!-- END Modal Header -->

				<!-- Modal Body -->
				<div class="modal-body">
					<form action="index.html" method="post"
						enctype="multipart/form-data"
						class="form-horizontal form-bordered" onsubmit="return false;">
						<fieldset>
							<legend>Vital Info</legend>
							<div class="form-group">
								<label class="col-md-4 control-label">Username</label>
								<div class="col-md-8">
									<p class="form-control-static">Admin</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="user-settings-email">Email</label>
								<div class="col-md-8">
									<input type="email" id="user-settings-email"
										name="user-settings-email" class="form-control"
										value="admin@example.com">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-notifications">Email Notifications</label>
								<div class="col-md-8">
									<label class="switch switch-primary"> <input
										type="checkbox" id="user-settings-notifications"
										name="user-settings-notifications" value="1" checked>
										<span></span>
									</label>
								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend>Password Update</legend>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password">New Password</label>
								<div class="col-md-8">
									<input type="password" id="user-settings-password"
										name="user-settings-password" class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword">Confirm New Password</label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="user-settings-repassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-sm btn-primary">Save
									Changes</button>
							</div>
						</div>
					</form>
				</div>
				<!-- END Modal Body -->
			</div>
		</div>
	</div>
	<!-- END User Settings -->

	<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/app.js"></script>

	<!-- ckeditor.js, load it only in the page you would like to use CKEditor (it's a heavy plugin to include it with the others!) -->
	<script src="js/helpers/ckeditor/ckeditor.js"></script>
</body>
</html>