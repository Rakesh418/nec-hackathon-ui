<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Admin Console for Amazon</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=0">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="img/favicon.png">
<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
<link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Related styles of various icon packs and plugins -->
<link rel="stylesheet" href="css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="css/main.css">

<!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
<link rel="stylesheet" href="css/themes.css">
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) -->
<script src="js/vendor/modernizr.min.js"></script>
</head>
<body>
	<!-- Page Wrapper -->
	<!-- In the PHP version you can set the following options from inc/config file -->
	<!--
            Available classes:

            'page-loading'      enables page preloader
        -->
	<div id="page-wrapper">
		<!-- Page Container -->
		<div id="page-container"
			class="sidebar-partial sidebar-visible-lg sidebar-no-animations">

			<!-- Main Sidebar -->
			<div id="sidebar">
				<!-- Wrapper for scrolling functionality -->
				<div id="sidebar-scroll">
					<!-- Sidebar Content -->
					<div class="sidebar-content">
						<!-- Brand -->
						<a href="/dashboard" class="sidebar-brand"> <i
							class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Admin</strong>
								Console</span>
						</a>
						<!-- END Brand -->

						<!-- User Info -->
						<div
							class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
							<div class="sidebar-user-avatar">
								<img src="img/placeholders/avatars/avatar2.jpg" alt="avatar">
							</div>
							<div class="sidebar-user-name">Amazon</div>
							<div class="sidebar-user-links">
								<!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
								<a href="javascript:void(0)" class="enable-tooltip"
									data-placement="bottom" title="Settings"
									onclick="$('#modal-user-settings').modal('show');"><i
									class="gi gi-cogwheel"></i></a> <a href="login.html"
									data-toggle="tooltip" data-placement="bottom" title="Logout"><i
									class="gi gi-exit"></i></a>
							</div>
						</div>
						<!-- END User Info -->

						<!-- Theme Colors -->
						<!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
						<ul
							class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">
							<!-- You can also add the default color theme
                                    <li class="active">
                                        <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                                    </li>
                                    -->
							<li><a href="javascript:void(0)"
								class="themed-background-dark-night themed-border-night"
								data-theme="css/themes/night.css" data-toggle="tooltip"
								title="Night"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-amethyst themed-border-amethyst"
								data-theme="css/themes/amethyst.css" data-toggle="tooltip"
								title="Amethyst"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-modern themed-border-modern"
								data-theme="css/themes/modern.css" data-toggle="tooltip"
								title="Modern"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-autumn themed-border-autumn"
								data-theme="css/themes/autumn.css" data-toggle="tooltip"
								title="Autumn"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-flatie themed-border-flatie"
								data-theme="css/themes/flatie.css" data-toggle="tooltip"
								title="Flatie"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-spring themed-border-spring"
								data-theme="css/themes/spring.css" data-toggle="tooltip"
								title="Spring"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-fancy themed-border-fancy"
								data-theme="css/themes/fancy.css" data-toggle="tooltip"
								title="Fancy"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-fire themed-border-fire"
								data-theme="css/themes/fire.css" data-toggle="tooltip"
								title="Fire"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-coral themed-border-coral"
								data-theme="css/themes/coral.css" data-toggle="tooltip"
								title="Coral"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-lake themed-border-lake"
								data-theme="css/themes/lake.css" data-toggle="tooltip"
								title="Lake"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-forest themed-border-forest"
								data-theme="css/themes/forest.css" data-toggle="tooltip"
								title="Forest"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-waterlily themed-border-waterlily"
								data-theme="css/themes/waterlily.css" data-toggle="tooltip"
								title="Waterlily"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-emerald themed-border-emerald"
								data-theme="css/themes/emerald.css" data-toggle="tooltip"
								title="Emerald"></a></li>
							<li><a href="javascript:void(0)"
								class="themed-background-dark-blackberry themed-border-blackberry"
								data-theme="css/themes/blackberry.css" data-toggle="tooltip"
								title="Blackberry"></a></li>
						</ul>
						<!-- END Theme Colors -->


						<!-- Sidebar Navigation -->
						<ul class="sidebar-nav">
							<li><a href="/dashboard"><i
									class="gi gi-stopwatch sidebar-nav-icon"></i><span
									class="sidebar-nav-mini-hide">Dashboard</span></a></li>

							<li><a href="/orders" class=" active">Orders</a></li>
							<li><a href="">Order View</a></li>
							<li><a href="/create_courier">Create Courier</a></li>
						</ul>
						<!-- END Sidebar Navigation -->
					</div>
					<!-- END Sidebar Content -->
				</div>
				<!-- END Wrapper for scrolling functionality -->
			</div>
			<!-- END Main Sidebar -->

			<!-- Main Container -->
			<div id="main-container">
				<!-- Header -->
				<!-- In the PHP version you can set the following options from inc/config file -->
				<!--
                            Available header.navbar classes:
    
                            'navbar-default'            for the default light header
                            'navbar-inverse'            for an alternative dark header
    
                            'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                                'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added
    
                            'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                                'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                        -->
				<header class="navbar navbar-default">
					<!-- Left Header Navigation -->
					<ul class="nav navbar-nav-custom">
						<!-- Main Sidebar Toggle Button -->
						<li><a href="javascript:void(0)"
							onclick="App.sidebar('toggle-sidebar');this.blur();"> <i
								class="fa fa-bars fa-fw"></i>
						</a></li>
						<!-- END Main Sidebar Toggle Button -->

						<!-- Template Options -->
						<!-- Change Options functionality can be found in js/app.js - templateOptions() -->
						<li class="dropdown"><a href="javascript:void(0)"
							class="dropdown-toggle" data-toggle="dropdown"> <i
								class="gi gi-settings"></i>
						</a>
							<ul class="dropdown-menu dropdown-custom dropdown-options">
								<li class="dropdown-header text-center">Header Style</li>
								<li>
									<div class="btn-group btn-group-justified btn-group-sm">
										<a href="javascript:void(0)" class="btn btn-primary"
											id="options-header-default">Light</a> <a
											href="javascript:void(0)" class="btn btn-primary"
											id="options-header-inverse">Dark</a>
									</div>
								</li>
								<li class="dropdown-header text-center">Page Style</li>
								<li>
									<div class="btn-group btn-group-justified btn-group-sm">
										<a href="javascript:void(0)" class="btn btn-primary"
											id="options-main-style">Default</a> <a
											href="javascript:void(0)" class="btn btn-primary"
											id="options-main-style-alt">Alternative</a>
									</div>
								</li>
							</ul></li>
						<!-- END Template Options -->
					</ul>
					<!-- END Left Header Navigation -->

					<!-- Search Form -->
					<form action="page_ready_search_results.html" method="post"
						class="navbar-form-custom">
						<div class="form-group">
							<input type="text" id="top-search" name="top-search"
								class="form-control" placeholder="Search..">
						</div>
					</form>
					<!-- END Search Form -->

					<!-- Right Header Navigation -->
					<ul class="nav navbar-nav-custom pull-right">
					</ul>
					<!-- END Right Header Navigation -->
				</header>
				<!-- END Header -->

				<!-- Page content -->
				<div id="page-content">
					<!-- Dashboard Header -->
					<div class="content-header">
						<ul class="nav-horizontal text-center">
							<li><a href="/dashboard"><i class="fa fa-bar-chart"></i>
									Dashboard</a></li>
							<li class=" active"><a href="/orders"><i
									class="gi gi-shop_window"></i> Orders</a></li>
							<li><a href=""><i
									class="gi gi-shopping_cart"></i> Order View</a></li>
							<li><a href="/create_courier"><i
									class="gi gi-pencil"></i> Create Courier</a></li>
						</ul>
					</div>
					<!-- END Dashboard Header -->

					<!-- Quick Stats -->
					<div class="row text-center">
						<div class="col-sm-6 col-lg-3">
							<a href="javascript:void(0)" class="widget widget-hover-effect2">
								<div class="widget-extra themed-background">
									<h4 class="widget-content-light">
										<strong>Total</strong> Couriers
									</h4>
								</div>
								<div class="widget-extra-full">
									<span class="h2 animation-expandOpen">${totalCourier}</span>
								</div>
							</a>
						</div>
						<div class="col-sm-6 col-lg-3">
							<a href="javascript:void(0)" class="widget widget-hover-effect2">
								<div class="widget-extra themed-background-dark">
									<h4 class="widget-content-light">
										<strong>Response</strong> Received
									</h4>
								</div>
								<div class="widget-extra-full">
									<span class="h2 themed-color-dark animation-expandOpen">${responseReceived}</span>
								</div>
							</a>
						</div>
						<div class="col-sm-6 col-lg-3">
							<a href="javascript:void(0)" class="widget widget-hover-effect2">
								<div class="widget-extra themed-background-dark">
									<h4 class="widget-content-light">
										<strong>Accepted</strong> Couriers
									</h4>
								</div>
								<div class="widget-extra-full">
									<span class="h2 themed-color-dark animation-expandOpen">${acceptedCourier}</span>
								</div>
							</a>
						</div>
						<div class="col-sm-6 col-lg-3">
							<a href="javascript:void(0)" class="widget widget-hover-effect2">
								<div class="widget-extra themed-background-dark">
									<h4 class="widget-content-light">
										<strong>Delivered</strong> Today
									</h4>
								</div>
								<div class="widget-extra-full">
									<span class="h2 themed-color-dark animation-expandOpen">${delivered}</span>
								</div>
							</a>
						</div>
					</div>
					<!-- END Quick Stats -->


					<!-- All Orders Block -->
					<div class="block full">
						<!-- All Orders Title -->
						<div class="block-title">
							<div class="block-options pull-right">
								<a href="javascript:void(0)"
									class="btn btn-alt btn-sm btn-default" data-toggle="tooltip"
									title="Settings"><i class="fa fa-cog"></i></a>
							</div>
							<h2>
								<strong>All</strong> Orders
							</h2>
						</div>
						<!-- END All Orders Title -->

						<!-- All Orders Content -->
						<table id="ecom-orders"
							class="table table-bordered table-striped table-vcenter">
							<thead>
								<tr>
									<th class="visible-lg" style="width: 300px;">CourierId</th>
									<th class="text-center visible-lg">Source</th>
									<th class="text-center visible-lg">Destination</th>
									<th class="text-center visible-lg">Dimensions</th>
									<th class="hidden-xs text-center">Weight</th>
									<th class="text-center visible-lg">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list}" var="temp">
									<tr>
										<td class="text-center" style="width: 300px;"><a
											href="/courier-details?courierId=${temp.state.data.courierId}"><strong>${temp.state.data.courierId}</strong></a></td>
										<td class="visible-lg">${temp.state.data.source}</td>
										<td class="text-center visible-lg">${temp.state.data.destination}</td>
										<td class="text-center visible-lg">${temp.state.data.courierLength}x
											${temp.state.data.courierWidth} x
											${temp.state.data.courierHeight}</td>
										<td class="hidden-xs text-center"><strong>${temp.state.data.courierWeight}</strong></td>
										<td class="text-center visible-lg"><c:choose>
												<c:when test="${temp.state.data.status == 'initiated'}">
													<span class="label label-default">${temp.state.data.status}</span>
												</c:when>
												<c:when test="${temp.state.data.status == 'delivered'}">
													<span class="label label-success">${temp.state.data.status}</span>
												</c:when>
												<c:when
													test="${temp.state.data.status == 'requestor-cancelled'}">
													<span class="label label-danger">${temp.state.data.status}</span>
												</c:when>
												<c:when
													test="${temp.state.data.status == 'responder-cancelled'}">
													<span class="label label-danger">${temp.state.data.status}</span>
												</c:when>
												<c:otherwise>
													<span class="label label-warning">${temp.state.data.status}</span>
												</c:otherwise>
											</c:choose>
										<td class="text-center">
											<div class="btn-group btn-group-xs">

												<a
													href="/courier-details?courierId=${temp.state.data.courierId}"
													data-toggle="tooltip" title="View" class="btn btn-default"><i
													class="fa fa-eye"></i></a>
											</div>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<!-- END All Orders Content -->
					</div>
					<!-- END All Orders Block -->
				</div>
				<!-- END Page Content -->

				<!-- Footer -->
				<footer class="clearfix"> </footer>
				<!-- END Footer -->
			</div>
			<!-- END Main Container -->
		</div>
		<!-- END Page Container -->
	</div>
	<!-- END Page Wrapper -->

	<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
	<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

	<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
	<div id="modal-user-settings" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header text-center">
					<h2 class="modal-title">
						<i class="fa fa-pencil"></i> Settings
					</h2>
				</div>
				<!-- END Modal Header -->

				<!-- Modal Body -->
				<div class="modal-body">
					<form action="index.html" method="post"
						enctype="multipart/form-data"
						class="form-horizontal form-bordered" onsubmit="return false;">
						<fieldset>
							<legend>Vital Info</legend>
							<div class="form-group">
								<label class="col-md-4 control-label">Username</label>
								<div class="col-md-8">
									<p class="form-control-static">Admin</p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="user-settings-email">Email</label>
								<div class="col-md-8">
									<input type="email" id="user-settings-email"
										name="user-settings-email" class="form-control"
										value="admin@example.com">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-notifications">Email Notifications</label>
								<div class="col-md-8">
									<label class="switch switch-primary"> <input
										type="checkbox" id="user-settings-notifications"
										name="user-settings-notifications" value="1" checked>
										<span></span>
									</label>
								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend>Password Update</legend>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-password">New Password</label>
								<div class="col-md-8">
									<input type="password" id="user-settings-password"
										name="user-settings-password" class="form-control"
										placeholder="Please choose a complex one..">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label"
									for="user-settings-repassword">Confirm New Password</label>
								<div class="col-md-8">
									<input type="password" id="user-settings-repassword"
										name="user-settings-repassword" class="form-control"
										placeholder="..and confirm it!">
								</div>
							</div>
						</fieldset>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="button" class="btn btn-sm btn-default"
									data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-sm btn-primary">Save
									Changes</button>
							</div>
						</div>
					</form>
				</div>
				<!-- END Modal Body -->
			</div>
		</div>
	</div>
	<!-- END User Settings -->

	<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/app.js"></script>

	<!-- Load and execute javascript code used only in this page -->
	<script src="js/pages/ecomOrders.js"></script>
	<script>
		$(function() {
			EcomOrders.init();
		});
	</script>
</body>
</html>