package com.rakesh.nechackathon.controller;

import com.rakesh.nechackathon.util.ResponsesC;
import com.rakesh.nechackathon.util.UserDetails;
import com.rakesh.nechackathon.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.multipart.*;
import org.springframework.http.*;
import org.springframework.util.*;
import org.springframework.core.io.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@org.springframework.stereotype.Controller
public class AmazonController {

	private final Logger logger = LoggerFactory.getLogger(AmazonController.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private UserDetails userDetails;

	@Autowired
	private Utility utility;

	@RequestMapping("/login")
	public String loginPage() {
		logger.info("In login controller");
		return "login";
	}

	@Value("${ecommerceIPAddress}")
	private String ecommerceIPAddress;

	@RequestMapping("/logout-success")
	public String logoutPage() {
		String username = userDetails.getUserDetails();
		SecurityContextHolder.getContext().setAuthentication(null);
		logger.info("user {} has successfully logout from courier system", username);
		return "logout";
	}

	@RequestMapping("/home")
	public String home() {
		logger.info("user {} In home controller", userDetails.getUserDetails());
		return "home";
	}

	@RequestMapping("/create_courier")
	public String create_courier() {
		logger.info("user {} In create courier controller", userDetails.getUserDetails());
		return "create_courier";
	}

	@GetMapping("/orders")
	public String orders(Model model) {
		logger.info("user {} In orders controller", userDetails.getUserDetails());
		Map<String, String> vars = new HashMap<>();
		String dashboardUrl = new StringBuilder().append(ecommerceIPAddress).append("/api/couriers/any/getAll")
				.toString();
		Object[] orderList = restTemplate.getForObject(dashboardUrl, Object[].class, vars);

		try {
			int responseReceived = 0;
			int acceptedCourier = 0;
			int delivered = 0;

			if (orderList != null) {
				model.addAttribute("totalCourier", orderList.length);
				for (int i = 0; i < orderList.length; i++) {
					Map<String, Object> tempResponse = (Map<String, Object>) orderList[i];
					Map<String, Object> data = (Map<String, Object>) ((LinkedHashMap) (((LinkedHashMap) tempResponse)
							.get("state"))).get("data");

					switch ((String) data.get("status")) {

					case "accepted":
						acceptedCourier++;
						break;
					case "response-received":
						responseReceived++;
						break;
					case "delivered":
						delivered++;
						break;
					}
				}
			} else {

				model.addAttribute("totalCourier", 0);
			}
			model.addAttribute("responseReceived", responseReceived);
			model.addAttribute("acceptedCourier", acceptedCourier);
			model.addAttribute("delivered", delivered);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("totalCourier", 0);
			model.addAttribute("responseReceived", 0);
			model.addAttribute("acceptedCourier", 0);
			model.addAttribute("delivered", 0);
		}

		System.out.println("success-message" + model.asMap().get("success-message"));
//        List<Map<String,String>> orderList= utilTest.getAmazonDashboard();
		model.addAttribute("list", orderList);
		return "orders";
	}

	@GetMapping("/dashboard")
	public String dashboard(Model model) {
		logger.info("user {} In dashboard controller", userDetails.getUserDetails());
		Map<String, String> vars = new HashMap<>();
		String dashboardUrl = new StringBuilder().append(ecommerceIPAddress).append("/api/couriers/any/getAll")
				.toString();
		Object[] orderList = restTemplate.getForObject(dashboardUrl, Object[].class, vars);

		try {
			int responseReceived = 0;
			int acceptedCourier = 0;
			int delivered = 0;

			if (orderList != null) {
				model.addAttribute("totalCourier", orderList.length);
				for (int i = 0; i < orderList.length; i++) {
					Map<String, Object> tempResponse = (Map<String, Object>) orderList[i];
					Map<String, Object> data = (Map<String, Object>) ((LinkedHashMap) (((LinkedHashMap) tempResponse)
							.get("state"))).get("data");

					switch ((String) data.get("status")) {

					case "accepted":
						acceptedCourier++;
						break;
					case "response-received":
						responseReceived++;
						break;
					case "delivered":
						delivered++;
						break;
					}
				}
			} else {

				model.addAttribute("totalCourier", 0);
			}
			model.addAttribute("responseReceived", responseReceived);
			model.addAttribute("acceptedCourier", acceptedCourier);
			model.addAttribute("delivered", delivered);
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("totalCourier", 0);
			model.addAttribute("responseReceived", 0);
			model.addAttribute("acceptedCourier", 0);
			model.addAttribute("delivered", 0);
		}

		System.out.println("success-message" + model.asMap().get("success-message"));
//        List<Map<String,String>> orderList= utilTest.getAmazonDashboard();
		model.addAttribute("list", orderList);
		return "dashboard3";
	}

//    @RequestMapping("/register-order")
	@PostMapping("/register-order")
	public String registerOrder(Model model, HttpServletRequest request) {
		logger.info("user {} In register-order controller", userDetails.getUserDetails());
		String courierLength = request.getParameter("courierLength");
		String courierWidth = request.getParameter("courierWidth");
		String courierHeight = request.getParameter("courierHeight");
		String courierWeight = request.getParameter("courierWeight");
		String source = request.getParameter("source");
		String destination = request.getParameter("destination");

//		Map<String, String> parameter = new HashMap<>();
//		parameter.put("courierLength", courierLength);
//		parameter.put("courierWidth", courierWidth);
//		parameter.put("courierHeight", courierHeight);
//		parameter.put("courierWeight", courierWeight);
//		parameter.put("source", source);
//		parameter.put("destination", destination);
//		parameter.put("partyName", "O=NECAuto,L=New%20York,C=US");
		// Object[] courierList = restTemplate.postForObject(new
		// StringBuilder().append(ecommerceIPAddress).append("/api/couriers/requestor/create").toString(),null,
		// Object[].class,parameter);

//        ResponseEntity<String> response = restTemplate.postForEntity(url, parameter, String.class);

		try {
			Map<String, String> vars = new HashMap<>();
			String courierCreateUrl = new StringBuilder().append(ecommerceIPAddress)
					.append("/api/couriers/requestor/create?courierLength=").append(courierLength)
					.append("&courierWidth=").append(courierWidth).append("&courierHeight=").append(courierHeight)
					.append("&courierWeight=").append(courierWeight).append("&source=").append(source)
					.append("&destination=").append(destination).append("&partyName=")
					.append("O=NECAuto, L=New York, C=US").toString();

			logger.info("courierCreateUrl " + courierCreateUrl);
			ResponseEntity<String> courierDetails = restTemplate.getForEntity(courierCreateUrl, String.class, vars);
			model.addAttribute("successmessage", "1");
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("successmessage", "2");

		}

		return dashboard(model);
	}

	@GetMapping("/courier-details")
	public String courierDetails(Model model, HttpServletRequest request) {
		logger.info("user {} In courier-details controller", userDetails.getUserDetails());
		String courierId = request.getParameter("courierId");
		String courierDetailsUrl = new StringBuilder().append(ecommerceIPAddress)
				.append("/api/couriers/any/getById?courierId=").append(courierId).toString();
		Map<String, String> vars = new HashMap<>();
		Object courierDetails = restTemplate.getForObject(courierDetailsUrl, Object.class, vars);
		model.addAttribute("courierDetails", courierDetails);
		logger.info("courierId={}, courierDetails={}", courierId, courierDetails);

		List<ResponsesC> data = utility.getAutoResponseDetails(courierDetails);
		model.addAttribute("responses", data);
		return "courier_details_new";
	}

	@PostMapping("/accept-reject")
	public String courierAcceptRejectFromAmazon(Model model, HttpServletRequest request) {
		logger.info("user {} In accept-reject controller", userDetails.getUserDetails());
		String approvalFlag = request.getParameter("approval");
		String courierId = request.getParameter("courierId");
		String responder = request.getParameter("responder");
		String finalDeliveryType = request.getParameter("finalDeliveryType");
		String courierCancelAcceptUrl = "";

		if (approvalFlag.equalsIgnoreCase("accept")) {
			courierCancelAcceptUrl = new StringBuilder().append(ecommerceIPAddress)
					.append("/api/couriers/requestor/acceptCourier?courierId=").append(courierId).append("&responder=")
					.append(responder).append("&finalDeliveryType=").append(finalDeliveryType).toString();
		} else if (approvalFlag.equalsIgnoreCase("reject")) {
			courierCancelAcceptUrl = new StringBuilder().append(ecommerceIPAddress)
					.append("/api/couriers/requestor/cancelByRequestor?courierId=").append(courierId).toString();
		}

		Map<String, String> vars = new HashMap<>();
		try {
			ResponseEntity<String> courierUpdate = restTemplate.getForEntity(courierCancelAcceptUrl, String.class, vars);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String courierDetailsUrl = new StringBuilder().append(ecommerceIPAddress)
				.append("/api/couriers/any/getById?courierId=").append(courierId).toString();
		Object courierDetails = restTemplate.getForObject(courierDetailsUrl, Object.class, vars);
		model.addAttribute("courierDetails", courierDetails);
		List<ResponsesC> data = utility.getAutoResponseDetails(courierDetails);
		model.addAttribute("responses", data);
		return "courier_details_new";
	}

	@PostMapping("/upload-image")
	public String uploadImage(Model model, HttpServletRequest request) {
		logger.info("user {} In upload-image controller with type {}", userDetails.getUserDetails(),
				request.getContentType());

		try {
			// List<Part> formData = new ArrayList(request.getParts());
			// Part part = formData.get(0);
			// Part part = request.getPart("file1");
			// String parameterName = part.getName();
			// logger.info("STORC IMAGES - " + parameterName);
			String courierId = request.getParameter("courierId");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

			Set set = multipartRequest.getFileMap().entrySet();
			Iterator i = set.iterator();
			while (i.hasNext()) {
				Map.Entry me = (Map.Entry) i.next();
				String fileName = (String) me.getKey();
				MultipartFile multipartFile = (MultipartFile) me.getValue();
				logger.info("Original fileName - " + multipartFile.getOriginalFilename());

				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.MULTIPART_FORM_DATA);
				MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
				body.add("file", new ByteArrayResource(multipartFile.getBytes()));
//				body.add("filename", multipartFile.getOriginalFilename());
				HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
//				ResponseEntity<String> response = restTemplate.postForEntity(
//						new StringBuilder().append(ecommerceIPAddress).append("/upload/attachment").toString(),
//						requestEntity, String.class);

				ResponseEntity<String> response = restTemplate.exchange(
						new StringBuilder().append(ecommerceIPAddress).append("/upload/attachment").toString(),
						HttpMethod.POST, requestEntity, String.class);

				logger.info("RESPONSE " + response.getBody());

				Map<String, String> vars = new HashMap<>();
				try {
					String courierDetailsUrl = new StringBuilder().append(ecommerceIPAddress)
							.append("/api/couriers/requestor/sendCourierReceiptHash?courierId=").append(courierId)
							.append("&courierReceiptHash=").append(response.getBody().trim()).toString();
					logger.info("recipt upload " + courierDetailsUrl);
					ResponseEntity<String> courierDetails = restTemplate.getForEntity(courierDetailsUrl, String.class, vars);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

//        Map<String,String> vars= new HashMap<>();
//        Object[] orderList = restTemplate.getForObject("http://localhost:8082/dashboard", Object[].class, vars);
//        List<Map<String,String>> orderList= utilTest.getAmazonDashboard();
//        model.addAttribute("list",orderList);
		return "dashboard";
	}
}
