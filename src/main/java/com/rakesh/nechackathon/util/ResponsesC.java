package com.rakesh.nechackathon.util;

public class ResponsesC {
    public String name;
    public String sharedPrice;
    public String dedicatedPrice;

    public ResponsesC(String name, String sharedPrice, String dedicatedPrice) {
        this.name = name;
        this.sharedPrice = sharedPrice;
        this.dedicatedPrice = dedicatedPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSharedPrice() {
        return sharedPrice;
    }

    public void setSharedPrice(String sharedPrice) {
        this.sharedPrice = sharedPrice;
    }

    public String getDedicatedPrice() {
        return dedicatedPrice;
    }

    public void setDedicatedPrice(String dedicatedPrice) {
        this.dedicatedPrice = dedicatedPrice;
    }

    @Override
    public String toString() {
        return "ResponsesC{" +
                "name='" + name + '\'' +
                ", sharedPrice='" + sharedPrice + '\'' +
                ", dedicatedPrice='" + dedicatedPrice + '\'' +
                '}';
    }
}
