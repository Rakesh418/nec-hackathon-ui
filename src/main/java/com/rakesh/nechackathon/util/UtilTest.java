package com.rakesh.nechackathon.util;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UtilTest {
    public List<Map<String,String>> getAmazonDashboard(){
        List<Map<String,String>> dashList = new ArrayList<>();

        HashMap<String, String> dashboard1 = new HashMap<>();
        dashboard1.put("source","blr1");
        dashboard1.put("destination","delhi1");
        dashboard1.put("dimension","15X34X241");
        dashboard1.put("weight","35g");
        dashboard1.put("status","initiated");

        HashMap<String, String> dashboard2 = new HashMap<>();
        dashboard2.put("source","blr2");
        dashboard2.put("destination","delhi2");
        dashboard2.put("dimension","15X34X242");
        dashboard2.put("weight","35g");
        dashboard2.put("status","initiated");

        HashMap<String, String> dashboard3 = new HashMap<>();
        dashboard3.put("source","blr3");
        dashboard3.put("destination","delhi3");
        dashboard3.put("dimension","15X34X243");
        dashboard3.put("weight","35g3");
        dashboard3.put("status","accepted");

        HashMap<String, String> dashboard4 = new HashMap<>();
        dashboard4.put("source","blr3");
        dashboard4.put("destination","delhi3");
        dashboard4.put("dimension","15X34X243");
        dashboard4.put("weight","35g3");
        dashboard4.put("status","initiated");

        HashMap<String, String> dashboard5 = new HashMap<>();
        dashboard5.put("source","blr3");
        dashboard5.put("destination","delhi3");
        dashboard5.put("dimension","15X34X243");
        dashboard5.put("weight","35g3");
        dashboard5.put("status","rejected");

        HashMap<String, String> dashboard6 = new HashMap<>();
        dashboard6.put("source","blr3");
        dashboard6.put("destination","delhi3");
        dashboard6.put("dimension","15X34X243");
        dashboard6.put("weight","35g3");
        dashboard6.put("status","accepted");

        HashMap<String, String> dashboard7 = new HashMap<>();
        dashboard7.put("source","blr3");
        dashboard7.put("destination","delhi3");
        dashboard7.put("dimension","15X34X243");
        dashboard7.put("weight","35g3");
        dashboard7.put("status","initiated");

        dashList.add(dashboard1);
        dashList.add(dashboard2);
        dashList.add(dashboard3);
//        dashList.add(dashboard4);
//        dashList.add(dashboard5);
//        dashList.add(dashboard6);
//        dashList.add(dashboard7);
        System.out.println(dashList);
        return dashList;
    }

    public Map<String,Object> getCourierDetails(){

        HashMap<String, Object> dashboard1 = new HashMap<>();
        dashboard1.put("source","blr1");
        dashboard1.put("destination","delhi1");
        dashboard1.put("length","15X34X241");
        dashboard1.put("height","15X34X241");
        dashboard1.put("width","35g");
        dashboard1.put("weight","35g");
//        dashboard1.put("status","initiated");
        dashboard1.put("status","accepted");
//        dashboard1.put("status","form-received");
//        dashboard1.put("status","pickedup");

        List<HashMap<String, String>> auto = new ArrayList<>();
        HashMap<String, String> auto1 = new HashMap<>();
        auto1.put("source","blr1");
        auto1.put("destination","delhi1");
        auto1.put("status","delhi1");

        HashMap<String, String> auto2 = new HashMap<>();
        auto2.put("source","blr1");
        auto2.put("destination","delhi1");
        auto2.put("status","form-received");

        auto.add(auto1);
        auto.add(auto2);

        dashboard1.put("auto",auto);
        System.out.println(dashboard1);
        return dashboard1;
    }
}
