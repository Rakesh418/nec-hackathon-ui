package com.rakesh.nechackathon.util;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class Utility {

	public List<ResponsesC> getAutoResponseDetails(Object courierDetails){
		List<ResponsesC> data = new ArrayList<>();
		try {
			LinkedHashMap<String, Object> a = (LinkedHashMap<String, Object>) ((LinkedHashMap) (((LinkedHashMap) courierDetails).get("state"))).get("data");
			LinkedHashMap<String, Object> responses = (LinkedHashMap<String, Object>) a.get("responses");
			if(responses != null) {
				for (Map.Entry<String, Object> temp : responses.entrySet()) {
					String[] split = temp.getValue().toString().split("-");
					data.add(new ResponsesC(temp.getKey(), split[0], split[1]));
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
}
